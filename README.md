# Small STM32-based game console.

Bill of Material: https://docs.google.com/spreadsheets/d/1IVCMvSLwRDJs63TqnnaobEMB2EvwbLpSPrkHYDLdR1I/edit#gid=1280805345

## Errata

### rev b.

* Audio amp too load / poorly filtered
* USB connector upside down
* A/B button positioning uncomfortable
* LEDs too visible
* Cranks / hall sensors unused
* Power switch awkward
* SWD header location awkward for right hand
* Comms connector awkward

### rev a.

* LCD flex connector pitch wrong
* LCD flex connector offset relative to body
* LCD flex connector mirrored
* SWD header mirrored
* Battery connector distance from edge
* Revision silkscreen text mirrored
* Crystal footprint too small
* Power switch pinout incorrect
* Power switch mounting pins not plated
* No SD data signal pullups
* Odd copper + mask cutout near SD
* Crystal load caps too large (value wise)
* Buttons too clicky
* Hall effect sensor part voltage range
