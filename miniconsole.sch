EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR019
U 1 1 5D3F3909
P 10350 3550
F 0 "#PWR019" H 10350 3300 50  0001 C CNN
F 1 "GND" V 10355 3422 50  0000 R CNN
F 2 "" H 10350 3550 50  0001 C CNN
F 3 "" H 10350 3550 50  0001 C CNN
	1    10350 3550
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR09
U 1 1 5D3F570F
P 5400 2000
F 0 "#PWR09" H 5400 1850 50  0001 C CNN
F 1 "+3V3" H 5415 2173 50  0000 C CNN
F 2 "" H 5400 2000 50  0001 C CNN
F 3 "" H 5400 2000 50  0001 C CNN
	1    5400 2000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5D3F631F
P 5400 6000
F 0 "#PWR010" H 5400 5750 50  0001 C CNN
F 1 "GND" H 5405 5827 50  0000 C CNN
F 2 "" H 5400 6000 50  0001 C CNN
F 3 "" H 5400 6000 50  0001 C CNN
	1    5400 6000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR018
U 1 1 5D3F7073
P 8750 2700
F 0 "#PWR018" H 8750 2450 50  0001 C CNN
F 1 "GND" H 8755 2527 50  0000 C CNN
F 2 "" H 8750 2700 50  0001 C CNN
F 3 "" H 8750 2700 50  0001 C CNN
	1    8750 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	10350 3550 10200 3550
Wire Wire Line
	10200 5350 10100 5350
Connection ~ 10200 3550
Wire Wire Line
	10200 3550 10100 3550
Wire Wire Line
	10100 5050 10200 5050
Wire Wire Line
	10100 4750 10200 4750
Wire Wire Line
	10100 4450 10200 4450
Text Label 7800 1900 0    50   ~ 0
SD_CMD
Text Label 7800 2100 0    50   ~ 0
SD_CLK
Text Label 7800 2300 0    50   ~ 0
SD_DAT0
Wire Wire Line
	5200 2250 5200 2150
Wire Wire Line
	5200 2150 5300 2150
Wire Wire Line
	5600 2150 5600 2250
Wire Wire Line
	5500 2250 5500 2150
Connection ~ 5500 2150
Wire Wire Line
	5500 2150 5600 2150
Wire Wire Line
	5400 2250 5400 2150
Connection ~ 5400 2150
Wire Wire Line
	5400 2150 5500 2150
Wire Wire Line
	5300 2250 5300 2150
Connection ~ 5300 2150
Wire Wire Line
	5300 2150 5400 2150
Wire Wire Line
	5400 2150 5400 2000
Wire Wire Line
	5200 5850 5200 5950
Wire Wire Line
	5200 5950 5300 5950
Wire Wire Line
	5400 5950 5400 6000
Wire Wire Line
	5400 5950 5500 5950
Wire Wire Line
	5600 5950 5600 5850
Connection ~ 5400 5950
Wire Wire Line
	5400 5850 5400 5950
Wire Wire Line
	5500 5850 5500 5950
Connection ~ 5500 5950
Wire Wire Line
	5500 5950 5600 5950
Wire Wire Line
	5300 5850 5300 5950
Connection ~ 5300 5950
Wire Wire Line
	5300 5950 5400 5950
Wire Wire Line
	4700 4050 4000 4050
Wire Wire Line
	4700 5350 4000 5350
Text Label 4350 5350 0    50   ~ 0
SD_CLK
Text Label 7800 2400 0    50   ~ 0
SD_DAT1
Text Label 7800 1800 0    50   ~ 0
SD_DAT3
Text Label 7800 1700 0    50   ~ 0
SD_DAT2
Wire Wire Line
	4700 5250 4000 5250
Wire Wire Line
	4700 5150 4000 5150
Text Label 4350 5150 0    50   ~ 0
SD_DAT2
Text Label 4350 5250 0    50   ~ 0
SD_DAT3
Wire Wire Line
	4700 5050 4000 5050
Wire Wire Line
	4700 4950 4000 4950
Text Label 4350 5050 0    50   ~ 0
SD_DAT1
Text Label 4350 4950 0    50   ~ 0
SD_DAT0
Wire Wire Line
	6000 3650 6300 3650
Wire Wire Line
	6000 3550 6300 3550
$Comp
L power:VBUS #PWR012
U 1 1 5D43842F
P 8600 4400
F 0 "#PWR012" H 8600 4250 50  0001 C CNN
F 1 "VBUS" V 8615 4528 50  0000 L CNN
F 2 "" H 8600 4400 50  0001 C CNN
F 3 "" H 8600 4400 50  0001 C CNN
	1    8600 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3850 4200 3850
Wire Wire Line
	4200 3850 4200 3800
Wire Wire Line
	4700 3750 4600 3750
Wire Wire Line
	4600 3750 4600 3450
Wire Wire Line
	4600 3450 4200 3450
Wire Wire Line
	4200 3450 4200 3500
Wire Wire Line
	4700 2450 4400 2450
Wire Wire Line
	6000 3850 6300 3850
Wire Wire Line
	6000 5150 6300 5150
Wire Wire Line
	6000 3750 6300 3750
$Comp
L Device:R R2
U 1 1 5D47391D
P 3650 2900
F 0 "R2" V 3443 2900 50  0000 C CNN
F 1 "10K" V 3534 2900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3580 2900 50  0001 C CNN
F 3 "~" H 3650 2900 50  0001 C CNN
	1    3650 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 3050 3650 3100
$Comp
L power:GND #PWR04
U 1 1 5D47744A
P 3650 3100
F 0 "#PWR04" H 3650 2850 50  0001 C CNN
F 1 "GND" V 3655 2972 50  0000 R CNN
F 2 "" H 3650 3100 50  0001 C CNN
F 3 "" H 3650 3100 50  0001 C CNN
	1    3650 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 2850 6300 2850
Wire Wire Line
	6000 2950 6300 2950
Wire Wire Line
	6000 3050 6300 3050
Wire Wire Line
	6000 3150 6300 3150
$Comp
L Switch:SW_Push SW1
U 1 1 5D4B28B0
P 9900 3550
F 0 "SW1" H 9900 3743 50  0000 C CNN
F 1 "SW_Push" H 9900 3744 50  0001 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_6x6mm_H9.5mm" H 9900 3750 50  0001 C CNN
F 3 "~" H 9900 3750 50  0001 C CNN
	1    9900 3550
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW4
U 1 1 5D4BBD9B
P 9900 4450
F 0 "SW4" H 9900 4643 50  0000 C CNN
F 1 "SW_Push" H 9900 4644 50  0001 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_6x6mm_H9.5mm" H 9900 4650 50  0001 C CNN
F 3 "~" H 9900 4650 50  0001 C CNN
	1    9900 4450
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW5
U 1 1 5D4BFAD9
P 9900 4750
F 0 "SW5" H 9900 4943 50  0000 C CNN
F 1 "SW_Push" H 9900 4944 50  0001 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_6x6mm_H9.5mm" H 9900 4950 50  0001 C CNN
F 3 "~" H 9900 4950 50  0001 C CNN
	1    9900 4750
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW6
U 1 1 5D4C73DC
P 9900 5050
F 0 "SW6" H 9900 5243 50  0000 C CNN
F 1 "SW_Push" H 9900 5244 50  0001 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_6x6mm_H9.5mm" H 9900 5250 50  0001 C CNN
F 3 "~" H 9900 5250 50  0001 C CNN
	1    9900 5050
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW7
U 1 1 5D4CAF4C
P 9900 5350
F 0 "SW7" H 9900 5543 50  0000 C CNN
F 1 "SW_Push" H 9900 5544 50  0001 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_6x6mm_H9.5mm" H 9900 5550 50  0001 C CNN
F 3 "~" H 9900 5550 50  0001 C CNN
	1    9900 5350
	1    0    0    -1  
$EndComp
Entry Wire Line
	9450 3450 9550 3550
Entry Wire Line
	9450 5250 9550 5350
Entry Wire Line
	9450 4950 9550 5050
Entry Wire Line
	9450 4650 9550 4750
Wire Wire Line
	9700 4750 9550 4750
Wire Wire Line
	9700 5050 9550 5050
Wire Wire Line
	9700 5350 9550 5350
Wire Wire Line
	9700 3550 9550 3550
Text Label 9550 4750 0    50   ~ 0
SW_RT
Text Label 9550 5050 0    50   ~ 0
SW_A
Text Label 9550 4150 0    50   ~ 0
SW_ST
Wire Wire Line
	6000 5450 6700 5450
Text Label 6050 5450 0    50   ~ 0
SW_ST
Wire Wire Line
	6000 4750 6300 4750
Wire Wire Line
	6000 3450 6300 3450
Wire Wire Line
	3100 7000 3200 7000
Wire Wire Line
	6000 3250 6300 3250
Wire Wire Line
	6000 4550 6700 4550
Wire Wire Line
	6000 3950 6700 3950
Text Label 6050 4150 0    50   ~ 0
SW_A
Text Label 6050 4350 0    50   ~ 0
SW_RT
Wire Wire Line
	6000 4350 6700 4350
Wire Wire Line
	6000 4150 6700 4150
Text Label 6050 4550 0    50   ~ 0
SW_DN
Wire Wire Line
	6000 2550 6300 2550
Wire Wire Line
	4200 3850 4100 3850
Connection ~ 4200 3850
Wire Wire Line
	4200 3450 4100 3450
Connection ~ 4200 3450
Wire Wire Line
	3800 3450 3700 3450
Wire Wire Line
	3700 3850 3800 3850
$Comp
L power:GND #PWR05
U 1 1 5D5645B2
P 3700 3950
F 0 "#PWR05" H 3700 3700 50  0001 C CNN
F 1 "GND" H 3705 3777 50  0000 C CNN
F 2 "" H 3700 3950 50  0001 C CNN
F 3 "" H 3700 3950 50  0001 C CNN
	1    3700 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 3950 3700 3850
Connection ~ 3700 3850
Wire Wire Line
	6000 5250 6300 5250
NoConn ~ 4700 4650
Wire Wire Line
	4700 4850 4350 4850
$Comp
L Device:R R3
U 1 1 5D73F3A6
P 4400 2200
F 0 "R3" H 4330 2154 50  0000 R CNN
F 1 "10K" H 4330 2245 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4330 2200 50  0001 C CNN
F 3 "~" H 4400 2200 50  0001 C CNN
	1    4400 2200
	-1   0    0    1   
$EndComp
Wire Wire Line
	4400 2350 4400 2450
Wire Wire Line
	4400 2050 4400 1950
$Comp
L power:+3V3 #PWR07
U 1 1 5D750F2F
P 4400 1950
F 0 "#PWR07" H 4400 1800 50  0001 C CNN
F 1 "+3V3" H 4415 2123 50  0000 C CNN
F 2 "" H 4400 1950 50  0001 C CNN
F 3 "" H 4400 1950 50  0001 C CNN
	1    4400 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5D7513D3
P 8600 4750
F 0 "R7" V 8650 4600 50  0000 C CNN
F 1 "100" V 8600 4750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8530 4750 50  0001 C CNN
F 3 "~" H 8600 4750 50  0001 C CNN
	1    8600 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 4400 8600 4600
Wire Wire Line
	6000 2650 6300 2650
Text GLabel 6300 3250 2    50   Output ~ 0
DISP_PWM
Wire Bus Line
	7700 1500 7600 1500
Wire Wire Line
	8750 2700 8750 2600
Wire Wire Line
	8750 2600 8750 2200
Connection ~ 8750 2600
Wire Wire Line
	8750 2000 8750 1500
$Comp
L power:+3V3 #PWR017
U 1 1 5D859BF1
P 8750 1500
F 0 "#PWR017" H 8750 1350 50  0001 C CNN
F 1 "+3V3" H 8850 1650 50  0000 C CNN
F 2 "" H 8750 1500 50  0001 C CNN
F 3 "" H 8750 1500 50  0001 C CNN
	1    8750 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	10750 2600 10600 2600
$Comp
L power:GND #PWR020
U 1 1 5D86CEFE
P 10750 2700
F 0 "#PWR020" H 10750 2450 50  0001 C CNN
F 1 "GND" H 10755 2527 50  0000 C CNN
F 2 "" H 10750 2700 50  0001 C CNN
F 3 "" H 10750 2700 50  0001 C CNN
	1    10750 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	10750 2600 10750 2700
Entry Wire Line
	7700 1600 7800 1700
Entry Wire Line
	7700 1700 7800 1800
Entry Wire Line
	7700 2300 7800 2400
Entry Wire Line
	7700 2200 7800 2300
Entry Wire Line
	7700 2000 7800 2100
Entry Wire Line
	7700 1800 7800 1900
Wire Wire Line
	8900 2600 8750 2600
Wire Wire Line
	8900 2400 8600 2400
Wire Wire Line
	8900 2300 8500 2300
Wire Wire Line
	8900 2200 8750 2200
Wire Wire Line
	8900 2100 7800 2100
Wire Wire Line
	8900 2000 8750 2000
Wire Wire Line
	8900 1900 8400 1900
Wire Wire Line
	8900 2500 8100 2500
$Comp
L Connector:Micro_SD_Card_Det J3
U 1 1 5D3E4932
P 9800 2100
F 0 "J3" H 9750 2917 50  0000 C CNN
F 1 "Micro_SD_Card_Det" H 9750 2826 50  0000 C CNN
F 2 "Connector_Card:microSD_HC_Hirose_DM3BT-DSF-PEJS" H 11850 2800 50  0001 C CNN
F 3 "https://www.hirose.com/product/en/download_file/key_name/DM3/category/Catalog/doc_file_id/49662/?file_category_id=4&item_id=195&is_series=1" H 9800 2200 50  0001 C CNN
	1    9800 2100
	1    0    0    -1  
$EndComp
Entry Wire Line
	3900 5050 4000 4950
Entry Wire Line
	3900 5150 4000 5050
Entry Wire Line
	3900 5250 4000 5150
Entry Wire Line
	3900 5350 4000 5250
Entry Wire Line
	3900 5450 4000 5350
Text Label 4350 4050 0    50   ~ 0
SD_CMD
$Comp
L Switch:SW_Push SW3
U 1 1 5D4B842D
P 9900 4150
F 0 "SW3" H 9900 4350 50  0000 C CNN
F 1 "SW_Push" H 9900 4344 50  0001 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_6x6mm_H9.5mm" H 9900 4350 50  0001 C CNN
F 3 "~" H 9900 4350 50  0001 C CNN
	1    9900 4150
	1    0    0    -1  
$EndComp
Text Label 9550 5350 0    50   ~ 0
SW_LF
Wire Wire Line
	9700 4150 9550 4150
Entry Wire Line
	9450 4050 9550 4150
Wire Wire Line
	10100 4150 10200 4150
Wire Wire Line
	10200 3550 10200 3850
Text Label 9550 3550 0    50   ~ 0
SW_DN
Wire Wire Line
	10100 3850 10200 3850
Connection ~ 10200 3850
Connection ~ 10200 4150
Wire Wire Line
	10200 4150 10200 4450
Connection ~ 10200 4450
Wire Wire Line
	10200 4450 10200 4750
Connection ~ 10200 4750
Wire Wire Line
	10200 4750 10200 5050
Connection ~ 10200 5050
Wire Wire Line
	10200 5050 10200 5350
Wire Bus Line
	9450 3350 9350 3350
Wire Wire Line
	3650 2650 4700 2650
Wire Wire Line
	4700 2850 4600 2850
Wire Wire Line
	4150 2850 4250 2850
$Comp
L power:GND #PWR06
U 1 1 5D583BE0
P 4150 2850
F 0 "#PWR06" H 4150 2600 50  0001 C CNN
F 1 "GND" V 4155 2722 50  0000 R CNN
F 2 "" H 4150 2850 50  0001 C CNN
F 3 "" H 4150 2850 50  0001 C CNN
	1    4150 2850
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR08
U 1 1 5D773AD9
P 2300 4300
F 0 "#PWR08" H 2300 4150 50  0001 C CNN
F 1 "+3V3" H 2315 4473 50  0000 C CNN
F 2 "" H 2300 4300 50  0001 C CNN
F 3 "" H 2300 4300 50  0001 C CNN
	1    2300 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 2950 4550 2950
Wire Wire Line
	4550 2950 4550 3050
Wire Wire Line
	1500 1200 1500 1100
Wire Wire Line
	1500 1100 1950 1100
Wire Wire Line
	2400 1100 2400 1200
Wire Wire Line
	1950 1100 1950 1200
Connection ~ 1950 1100
Wire Wire Line
	1950 1100 2400 1100
Wire Wire Line
	1500 1500 1500 1600
Wire Wire Line
	1500 1600 1950 1600
Wire Wire Line
	1950 1600 1950 1500
Wire Wire Line
	1950 1600 2400 1600
Wire Wire Line
	2400 1600 2400 1500
Connection ~ 1950 1600
Wire Wire Line
	1950 1600 1950 1750
Wire Wire Line
	1950 1100 1950 1000
$Comp
L power:GND #PWR02
U 1 1 5DB99C3F
P 1950 1750
F 0 "#PWR02" H 1950 1500 50  0001 C CNN
F 1 "GND" H 1955 1577 50  0000 C CNN
F 2 "" H 1950 1750 50  0001 C CNN
F 3 "" H 1950 1750 50  0001 C CNN
	1    1950 1750
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR01
U 1 1 5DB9A2B3
P 1950 1000
F 0 "#PWR01" H 1950 850 50  0001 C CNN
F 1 "+3V3" H 1965 1173 50  0000 C CNN
F 2 "" H 1950 1000 50  0001 C CNN
F 3 "" H 1950 1000 50  0001 C CNN
	1    1950 1000
	1    0    0    -1  
$EndComp
NoConn ~ 1400 3400
$Comp
L power:GND #PWR016
U 1 1 5D4618AE
P 1900 4050
F 0 "#PWR016" H 1900 3800 50  0001 C CNN
F 1 "GND" H 1905 3877 50  0000 C CNN
F 2 "" H 1900 4050 50  0001 C CNN
F 3 "" H 1900 4050 50  0001 C CNN
	1    1900 4050
	-1   0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR015
U 1 1 5D460F90
P 1900 2450
F 0 "#PWR015" H 1900 2300 50  0001 C CNN
F 1 "+3V3" H 1915 2623 50  0000 C CNN
F 2 "" H 1900 2450 50  0001 C CNN
F 3 "" H 1900 2450 50  0001 C CNN
	1    1900 2450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1900 2600 1900 2450
Connection ~ 1900 3950
Wire Wire Line
	1900 3950 1900 4050
Wire Wire Line
	2000 3950 2000 3800
Wire Wire Line
	1900 3950 2000 3950
Wire Wire Line
	1900 3800 1900 3950
$Comp
L Connector:Conn_ARM_JTAG_SWD_10 J2
U 1 1 5D3E6FF1
P 1900 3200
F 0 "J2" H 1456 3200 50  0000 R CNN
F 1 "Conn_ARM_JTAG_SWD_10" H 1457 3155 50  0001 R CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x05_P1.27mm_Vertical_SMD" H 1900 3200 50  0001 C CNN
F 3 "http://infocenter.arm.com/help/topic/com.arm.doc.ddi0314h/DDI0314H_coresight_components_trm.pdf" V 1550 1950 50  0001 C CNN
	1    1900 3200
	-1   0    0    -1  
$EndComp
Text GLabel 1100 3200 0    50   BiDi ~ 0
SWDIO
Text GLabel 1100 3100 0    50   Input ~ 0
SWDCLK
Text GLabel 1100 2900 0    50   Output ~ 0
NRST
Text GLabel 4300 2450 0    50   Input ~ 0
NRST
Wire Wire Line
	4400 2450 4300 2450
Connection ~ 4400 2450
Text GLabel 6300 3850 2    50   Input ~ 0
SWDCLK
Text GLabel 6300 3750 2    50   BiDi ~ 0
SWDIO
Entry Wire Line
	6700 4150 6800 4050
Entry Wire Line
	6700 4250 6800 4150
Entry Wire Line
	6700 4350 6800 4250
Entry Wire Line
	6700 4550 6800 4450
Entry Wire Line
	6700 3950 6800 3850
Text Label 6050 3950 0    50   ~ 0
SW_LF
Text Label 6050 4850 0    50   ~ 0
SW_UP
Wire Bus Line
	6800 3750 7050 3750
Text GLabel 6300 4750 2    50   Output ~ 0
UART_TX
Text GLabel 6300 3450 2    50   Input ~ 0
UART_RX
Text GLabel 3200 7000 2    50   Input ~ 0
UART_TX
Text GLabel 3200 6900 2    50   Output ~ 0
UART_RX
Text GLabel 6300 5150 2    50   Output ~ 0
DISP_CS
Text GLabel 6300 5250 2    50   Output ~ 0
DISP_DC
Text GLabel 4350 4850 0    50   Input ~ 0
CHG_STAT
$Comp
L power:GND #PWR022
U 1 1 5DC7CF46
P 7000 1700
F 0 "#PWR022" H 7000 1450 50  0001 C CNN
F 1 "GND" H 7005 1527 50  0000 C CNN
F 2 "" H 7000 1700 50  0001 C CNN
F 3 "" H 7000 1700 50  0001 C CNN
	1    7000 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 1600 7000 1650
Wire Wire Line
	7000 1300 7000 1250
$Comp
L power:+3V3 #PWR021
U 1 1 5DC92DEA
P 7000 1200
F 0 "#PWR021" H 7000 1050 50  0001 C CNN
F 1 "+3V3" H 7015 1373 50  0000 C CNN
F 2 "" H 7000 1200 50  0001 C CNN
F 3 "" H 7000 1200 50  0001 C CNN
	1    7000 1200
	1    0    0    -1  
$EndComp
Text GLabel 6300 3550 2    50   BiDi ~ 0
USB_D-
Text GLabel 6300 3650 2    50   BiDi ~ 0
USB_D+
$Comp
L Device:C C7
U 1 1 5DE39089
P 3950 3450
F 0 "C7" V 4100 3400 50  0000 C CNN
F 1 "C" V 3789 3450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3988 3300 50  0001 C CNN
F 3 "~" H 3950 3450 50  0001 C CNN
	1    3950 3450
	0    1    1    0   
$EndComp
$Comp
L Device:C C8
U 1 1 5DE5CC01
P 3950 3850
F 0 "C8" V 3800 3800 50  0000 C CNN
F 1 "C" V 4100 3850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3988 3700 50  0001 C CNN
F 3 "~" H 3950 3850 50  0001 C CNN
	1    3950 3850
	0    1    1    0   
$EndComp
$Comp
L Device:C C9
U 1 1 5DE68B26
P 4400 2850
F 0 "C9" V 4250 2750 50  0000 C CNN
F 1 "2.2uF" V 4550 2800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4438 2700 50  0001 C CNN
F 3 "~" H 4400 2850 50  0001 C CNN
	1    4400 2850
	0    1    1    0   
$EndComp
$Comp
L Device:C C11
U 1 1 5DE807C2
P 7000 1450
F 0 "C11" H 6885 1404 50  0000 R CNN
F 1 "0.1uF" H 6885 1495 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7038 1300 50  0001 C CNN
F 3 "~" H 7000 1450 50  0001 C CNN
	1    7000 1450
	-1   0    0    1   
$EndComp
Text GLabel 6300 2850 2    50   Output ~ 0
SPK_PWM
Text GLabel 6300 2950 2    50   Output ~ 0
DISP_SCK
Text GLabel 6300 3050 2    50   Output ~ 0
DISP_RST
Text GLabel 6300 3150 2    50   Output ~ 0
DISP_MOSI
Text GLabel 6300 2650 2    50   Input ~ 0
BATSENSE
Wire Wire Line
	6000 2750 6300 2750
Text GLabel 8100 2500 0    50   Output ~ 0
SD_DETECT
Text GLabel 6300 2550 2    50   Input ~ 0
SD_DETECT
Wire Wire Line
	3650 2650 3650 2750
Connection ~ 3650 2650
$Comp
L Device:C C4
U 1 1 5E06E214
P 2400 1350
F 0 "C4" H 2285 1304 50  0000 R CNN
F 1 "0.1uF" H 2285 1395 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2438 1200 50  0001 C CNN
F 3 "~" H 2400 1350 50  0001 C CNN
	1    2400 1350
	-1   0    0    1   
$EndComp
$Comp
L Device:C C3
U 1 1 5E0890BC
P 1950 1350
F 0 "C3" H 1835 1304 50  0000 R CNN
F 1 "0.1uF" H 1835 1395 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1988 1200 50  0001 C CNN
F 3 "~" H 1950 1350 50  0001 C CNN
	1    1950 1350
	-1   0    0    1   
$EndComp
$Comp
L Device:C C2
U 1 1 5E089455
P 1500 1350
F 0 "C2" H 1385 1304 50  0000 R CNN
F 1 "0.1uF" H 1385 1395 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1538 1200 50  0001 C CNN
F 3 "~" H 1500 1350 50  0001 C CNN
	1    1500 1350
	-1   0    0    1   
$EndComp
$Comp
L Device:R R4
U 1 1 5E2D1A52
P 4900 6850
F 0 "R4" V 4800 6950 50  0000 C CNN
F 1 "1K" V 4800 6750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4830 6850 50  0001 C CNN
F 3 "~" H 4900 6850 50  0001 C CNN
	1    4900 6850
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C1
U 1 1 5E815A79
P 1050 1350
F 0 "C1" H 935 1304 50  0000 R CNN
F 1 "0.1uF" H 935 1395 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1088 1200 50  0001 C CNN
F 3 "~" H 1050 1350 50  0001 C CNN
	1    1050 1350
	-1   0    0    1   
$EndComp
$Comp
L Device:C C5
U 1 1 5EADB76C
P 2850 1350
F 0 "C5" H 2735 1304 50  0000 R CNN
F 1 "0.1uF" H 2735 1395 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2888 1200 50  0001 C CNN
F 3 "~" H 2850 1350 50  0001 C CNN
	1    2850 1350
	-1   0    0    1   
$EndComp
Wire Wire Line
	2400 1600 2850 1600
Wire Wire Line
	2850 1600 2850 1500
Connection ~ 2400 1600
Wire Wire Line
	2850 1200 2850 1100
Wire Wire Line
	2850 1100 2400 1100
Connection ~ 2400 1100
$Comp
L Mechanical:MountingHole H1
U 1 1 5EBA2CD7
P 1200 6900
F 0 "H1" H 1300 6900 50  0000 L CNN
F 1 "MountingHole" H 1300 6855 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.1mm" H 1200 6900 50  0001 C CNN
F 3 "~" H 1200 6900 50  0001 C CNN
	1    1200 6900
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5EBA4007
P 1200 7150
F 0 "H2" H 1300 7150 50  0000 L CNN
F 1 "MountingHole" H 1300 7105 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.1mm" H 1200 7150 50  0001 C CNN
F 3 "~" H 1200 7150 50  0001 C CNN
	1    1200 7150
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5EBA4117
P 1500 7150
F 0 "H4" H 1600 7150 50  0000 L CNN
F 1 "MountingHole" H 1600 7105 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.1mm" H 1500 7150 50  0001 C CNN
F 3 "~" H 1500 7150 50  0001 C CNN
	1    1500 7150
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5EBA4272
P 1500 6900
F 0 "H3" H 1600 6900 50  0000 L CNN
F 1 "MountingHole" H 1600 6855 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.1mm" H 1500 6900 50  0001 C CNN
F 3 "~" H 1500 6900 50  0001 C CNN
	1    1500 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5F7BE96A
P 3350 1350
F 0 "C6" H 3235 1304 50  0000 R CNN
F 1 "0.1uF" H 3235 1395 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3388 1200 50  0001 C CNN
F 3 "~" H 3350 1350 50  0001 C CNN
	1    3350 1350
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5F911E45
P 3200 7100
F 0 "#PWR03" H 3200 6850 50  0001 C CNN
F 1 "GND" V 3205 6972 50  0000 R CNN
F 2 "" H 3200 7100 50  0001 C CNN
F 3 "" H 3200 7100 50  0001 C CNN
	1    3200 7100
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW9
U 1 1 5F9ACED4
P 9900 5950
F 0 "SW9" H 9900 6143 50  0000 C CNN
F 1 "SW_Push" H 9900 6144 50  0001 C CNN
F 2 "Button_Switch_SMD:Panasonic_EVQPUJ_EVQPUA" H 9900 6150 50  0001 C CNN
F 3 "~" H 9900 6150 50  0001 C CNN
	1    9900 5950
	-1   0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW8
U 1 1 5F9AD1B2
P 9900 5650
F 0 "SW8" H 9900 5843 50  0000 C CNN
F 1 "SW_Push" H 9900 5844 50  0001 C CNN
F 2 "Button_Switch_SMD:Panasonic_EVQPUJ_EVQPUA" H 9900 5850 50  0001 C CNN
F 3 "~" H 9900 5850 50  0001 C CNN
	1    9900 5650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10100 5650 10200 5650
Wire Wire Line
	10100 5950 10200 5950
Wire Wire Line
	9700 5950 9550 5950
Wire Wire Line
	9700 5650 9550 5650
Entry Wire Line
	9550 5650 9450 5550
Entry Wire Line
	9550 5950 9450 5850
Text Label 9550 5650 0    50   ~ 0
SW_X
Text Label 9550 5950 0    50   ~ 0
SW_Y
$Comp
L MCU_ST_STM32F7:STM32F722RCTx U1
U 1 1 5D3E180C
P 5400 4050
F 0 "U1" H 5850 5800 50  0000 C CNN
F 1 "STM32F722RCTx" H 6000 5950 50  0000 C CNN
F 2 "Package_QFP:LQFP-64_10x10mm_P0.5mm" H 4800 2350 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00330506.pdf" H 5400 4050 50  0001 C CNN
	1    5400 4050
	1    0    0    -1  
$EndComp
Entry Wire Line
	6700 4650 6800 4550
Text Label 6050 4650 0    50   ~ 0
SW_X
Text Label 6050 4250 0    50   ~ 0
SW_Y
Wire Wire Line
	6000 4650 6700 4650
Wire Wire Line
	6000 4250 6700 4250
Wire Wire Line
	6000 4850 6700 4850
Wire Wire Line
	4350 4750 4700 4750
Wire Bus Line
	3900 5550 3650 5550
Entry Wire Line
	3900 4150 4000 4050
NoConn ~ 4700 4450
NoConn ~ 4700 4350
Wire Wire Line
	10200 3850 10200 4150
$Comp
L Switch:SW_Push SW2
U 1 1 5D4B49A2
P 9900 3850
F 0 "SW2" H 9900 4043 50  0000 C CNN
F 1 "SW_Push" H 9900 4044 50  0001 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_6x6mm_H9.5mm" H 9900 4050 50  0001 C CNN
F 3 "~" H 9900 4050 50  0001 C CNN
	1    9900 3850
	1    0    0    -1  
$EndComp
Text Label 9550 4450 0    50   ~ 0
SW_B
Wire Wire Line
	9700 4450 9550 4450
Entry Wire Line
	9450 4350 9550 4450
Text Label 9550 3850 0    50   ~ 0
SW_UP
Wire Wire Line
	9700 3850 9550 3850
Entry Wire Line
	9450 3750 9550 3850
Wire Wire Line
	6700 5350 6000 5350
Text Label 6050 5350 0    50   ~ 0
SW_B
$Comp
L Device:Crystal Y1
U 1 1 5D87C0C9
P 4200 3650
F 0 "Y1" V 4200 3000 50  0000 L CNN
F 1 "Crystal_GND24" V 4100 2550 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_5032-2Pin_5.0x3.2mm_HandSoldering" H 4200 3650 50  0001 C CNN
F 3 "~" H 4200 3650 50  0001 C CNN
	1    4200 3650
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP8
U 1 1 5D904779
P 850 4550
F 0 "TP8" V 850 4738 50  0000 L CNN
F 1 "TestPoint" V 895 4738 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 1050 4550 50  0001 C CNN
F 3 "~" H 1050 4550 50  0001 C CNN
	1    850  4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5D905825
P 850 4650
F 0 "#PWR013" H 850 4400 50  0001 C CNN
F 1 "GND" H 855 4477 50  0000 C CNN
F 2 "" H 850 4650 50  0001 C CNN
F 3 "" H 850 4650 50  0001 C CNN
	1    850  4650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	850  4550 850  4650
Text GLabel 6300 2750 2    50   Output ~ 0
SPK_EN
Entry Wire Line
	6700 5350 6800 5250
$Comp
L Device:R R5
U 1 1 5D530939
P 5950 7050
F 0 "R5" V 6050 7150 50  0000 C CNN
F 1 "470" V 6050 6950 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5880 7050 50  0001 C CNN
F 3 "~" H 5950 7050 50  0001 C CNN
	1    5950 7050
	0    1    1    0   
$EndComp
Wire Wire Line
	7800 1700 8200 1700
Wire Wire Line
	7800 1800 8300 1800
$Comp
L Device:C C10
U 1 1 5DEB51A3
P 6650 1450
F 0 "C10" H 6765 1496 50  0000 L CNN
F 1 "1uF" H 6765 1405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6688 1300 50  0001 C CNN
F 3 "~" H 6650 1450 50  0001 C CNN
	1    6650 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 1250 6650 1250
Wire Wire Line
	6650 1250 6650 1300
Connection ~ 7000 1250
Wire Wire Line
	7000 1250 7000 1200
Wire Wire Line
	7000 1650 6650 1650
Wire Wire Line
	6650 1650 6650 1600
Connection ~ 7000 1650
Wire Wire Line
	7000 1650 7000 1700
Connection ~ 8200 1700
Wire Wire Line
	8200 1700 8900 1700
$Comp
L Device:R R11
U 1 1 5DF5DF8C
P 8500 1250
F 0 "R11" V 8000 1050 50  0000 C CNN
F 1 "47K" V 8450 1450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8430 1250 50  0001 C CNN
F 3 "~" H 8500 1250 50  0001 C CNN
	1    8500 1250
	-1   0    0    1   
$EndComp
Connection ~ 8300 1800
Wire Wire Line
	8300 1800 8900 1800
Connection ~ 8400 1900
Wire Wire Line
	8400 1900 7800 1900
Connection ~ 8500 2300
Wire Wire Line
	8500 2300 7800 2300
$Comp
L Device:R R12
U 1 1 5DFEFAE6
P 8600 1250
F 0 "R12" V 8100 1050 50  0000 C CNN
F 1 "47K" V 8550 1450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8530 1250 50  0001 C CNN
F 3 "~" H 8600 1250 50  0001 C CNN
	1    8600 1250
	-1   0    0    1   
$EndComp
Connection ~ 8600 2400
Wire Wire Line
	8600 2400 7800 2400
Wire Wire Line
	8200 1100 8200 1050
Wire Wire Line
	8200 1050 8300 1050
Wire Wire Line
	8600 1050 8600 1100
Wire Wire Line
	8500 1050 8500 1100
Connection ~ 8500 1050
Wire Wire Line
	8500 1050 8600 1050
Wire Wire Line
	8400 1050 8400 1100
Connection ~ 8400 1050
Wire Wire Line
	8400 1050 8500 1050
Wire Wire Line
	8300 1050 8300 1100
Connection ~ 8300 1050
Wire Wire Line
	8300 1050 8400 1050
$Comp
L Device:R R9
U 1 1 5DF5D733
P 8300 1250
F 0 "R9" V 7800 1050 50  0000 C CNN
F 1 "47K" V 8250 1450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8230 1250 50  0001 C CNN
F 3 "~" H 8300 1250 50  0001 C CNN
	1    8300 1250
	-1   0    0    1   
$EndComp
$Comp
L Device:R R10
U 1 1 5DF5DB9C
P 8400 1250
F 0 "R10" V 7900 1050 50  0000 C CNN
F 1 "47K" V 8350 1450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8330 1250 50  0001 C CNN
F 3 "~" H 8400 1250 50  0001 C CNN
	1    8400 1250
	-1   0    0    1   
$EndComp
$Comp
L Device:R R8
U 1 1 5DF1ED05
P 8200 1250
F 0 "R8" V 7700 1050 50  0000 C CNN
F 1 "47K" V 8150 1450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8130 1250 50  0001 C CNN
F 3 "~" H 8200 1250 50  0001 C CNN
	1    8200 1250
	-1   0    0    1   
$EndComp
$Comp
L power:+3V3 #PWR014
U 1 1 5E0FE127
P 8400 950
F 0 "#PWR014" H 8400 800 50  0001 C CNN
F 1 "+3V3" H 8415 1123 50  0000 C CNN
F 2 "" H 8400 950 50  0001 C CNN
F 3 "" H 8400 950 50  0001 C CNN
	1    8400 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 3450 3700 3850
Wire Wire Line
	1100 2900 1400 2900
Wire Wire Line
	1100 3100 1400 3100
Wire Wire Line
	1100 3200 1400 3200
Wire Wire Line
	5800 7050 5700 7050
Wire Wire Line
	5100 6850 5050 6850
Wire Wire Line
	5100 7050 4750 7050
Wire Wire Line
	6100 7050 6150 7050
Text GLabel 4350 4750 0    50   Output ~ 0
LED1
Wire Wire Line
	4750 6850 4700 6850
Text GLabel 6150 7050 2    50   Input ~ 0
LED1
$Comp
L Device:R R1
U 1 1 5EF47FED
P 3400 2650
F 0 "R1" V 3193 2650 50  0000 C CNN
F 1 "0" V 3284 2650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3330 2650 50  0001 C CNN
F 3 "~" H 3400 2650 50  0001 C CNN
	1    3400 2650
	0    1    1    0   
$EndComp
Wire Wire Line
	3550 2650 3650 2650
Wire Wire Line
	3250 2650 3150 2650
Text Label 3150 2650 2    50   ~ 0
SW_ST
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5F1841BF
P 4600 2850
F 0 "#FLG01" H 4600 2925 50  0001 C CNN
F 1 "PWR_FLAG" H 4600 3023 50  0000 C CNN
F 2 "" H 4600 2850 50  0001 C CNN
F 3 "~" H 4600 2850 50  0001 C CNN
	1    4600 2850
	1    0    0    -1  
$EndComp
Connection ~ 4600 2850
Wire Wire Line
	4600 2850 4550 2850
$Sheet
S 1250 5100 1150 1050
U 5EF570ED
F0 "Peripherals" 50
F1 "periph.sch" 50
$EndSheet
Wire Wire Line
	8400 950  8400 1050
Wire Wire Line
	2850 1600 3350 1600
Wire Wire Line
	3350 1600 3350 1500
Connection ~ 2850 1600
Wire Wire Line
	2850 1100 3350 1100
Wire Wire Line
	3350 1100 3350 1200
Connection ~ 2850 1100
Wire Wire Line
	1050 1500 1050 1600
Wire Wire Line
	1050 1600 1500 1600
Connection ~ 1500 1600
Wire Wire Line
	1500 1100 1050 1100
Wire Wire Line
	1050 1100 1050 1200
Connection ~ 1500 1100
Text GLabel 4700 6850 0    50   Input ~ 0
CHG_STAT
Wire Wire Line
	10200 5350 10200 5650
Connection ~ 10200 5350
Connection ~ 10200 5650
Wire Wire Line
	10200 5650 10200 5950
Wire Wire Line
	1400 3300 1100 3300
Text GLabel 1100 3300 0    50   Input ~ 0
SWO
Wire Wire Line
	6000 4450 6300 4450
Text GLabel 6300 4450 2    50   Output ~ 0
SWO
Wire Wire Line
	8600 1400 8600 2400
Wire Wire Line
	8500 1400 8500 2300
Wire Wire Line
	8400 1400 8400 1900
Wire Wire Line
	8300 1400 8300 1800
Wire Wire Line
	8200 1400 8200 1700
$Comp
L Device:R R25
U 1 1 5F622F06
P 2950 7000
F 0 "R25" V 2750 7100 50  0000 C CNN
F 1 "470" V 2750 6900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2880 7000 50  0001 C CNN
F 3 "~" H 2950 7000 50  0001 C CNN
	1    2950 7000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2800 7000 2700 7000
NoConn ~ 4700 5450
NoConn ~ 4700 5550
NoConn ~ 4700 5650
NoConn ~ 4700 4550
NoConn ~ 4700 4250
NoConn ~ 6000 2450
Wire Wire Line
	8600 5200 8600 4900
Wire Wire Line
	6000 3350 6300 3350
Text GLabel 6300 3350 2    50   Input ~ 0
VBUS_SNS
Text GLabel 8600 5200 3    50   Output ~ 0
VBUS_SNS
Entry Wire Line
	6700 4850 6800 4750
Entry Wire Line
	6800 5350 6700 5450
NoConn ~ 6000 5650
$Comp
L Device:LED_Dual_ACAC D1
U 1 1 5F58F636
P 5400 6950
F 0 "D1" H 5400 7375 50  0000 C CNN
F 1 "LED_Dual_AACC" H 5400 7284 50  0000 C CNN
F 2 "miniconsole:led_dual_easv2010" H 5430 6950 50  0001 C CNN
F 3 "~" H 5430 6950 50  0001 C CNN
	1    5400 6950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5700 6850 6100 6850
$Comp
L power:+BATT #PWR057
U 1 1 5F5D194D
P 6100 6850
AR Path="/5F5D194D" Ref="#PWR057"  Part="1" 
AR Path="/5EF570ED/5F5D194D" Ref="#PWR?"  Part="1" 
F 0 "#PWR057" H 6100 6700 50  0001 C CNN
F 1 "+BATT" H 6115 7023 50  0000 C CNN
F 2 "" H 6100 6850 50  0001 C CNN
F 3 "" H 6100 6850 50  0001 C CNN
	1    6100 6850
	0    1    1    0   
$EndComp
NoConn ~ 3450 4000
NoConn ~ 6000 5550
$Comp
L power:+3.3VADC #PWR0104
U 1 1 5F5D2517
P 4550 3050
F 0 "#PWR0104" H 4700 3000 50  0001 C CNN
F 1 "+3.3VADC" H 4565 3223 50  0000 C CNN
F 2 "" H 4550 3050 50  0001 C CNN
F 3 "" H 4550 3050 50  0001 C CNN
	1    4550 3050
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3VADC #PWR0105
U 1 1 5F5EB07A
P 3150 4300
F 0 "#PWR0105" H 3300 4250 50  0001 C CNN
F 1 "+3.3VADC" H 3165 4473 50  0000 C CNN
F 2 "" H 3150 4300 50  0001 C CNN
F 3 "" H 3150 4300 50  0001 C CNN
	1    3150 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:Ferrite_Bead_Small FB1
U 1 1 5F5F2432
P 2500 4400
AR Path="/5F5F2432" Ref="FB1"  Part="1" 
AR Path="/5EF570ED/5F5F2432" Ref="FB?"  Part="1" 
F 0 "FB1" V 2350 4450 50  0000 C CNN
F 1 "120" V 2350 4250 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2430 4400 50  0001 C CNN
F 3 "~" H 2500 4400 50  0001 C CNN
	1    2500 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	2300 4300 2300 4400
Wire Wire Line
	2300 4400 2400 4400
Wire Wire Line
	2600 4400 2750 4400
Wire Wire Line
	3150 4400 3150 4300
Wire Wire Line
	2750 4400 2750 4500
Connection ~ 2750 4400
$Comp
L Device:C C30
U 1 1 5F623B66
P 2750 4650
F 0 "C30" H 2635 4604 50  0000 R CNN
F 1 "0.1uF" H 2635 4695 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2788 4500 50  0001 C CNN
F 3 "~" H 2750 4650 50  0001 C CNN
	1    2750 4650
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5F624096
P 2750 4900
F 0 "#PWR0106" H 2750 4650 50  0001 C CNN
F 1 "GND" H 2755 4727 50  0000 C CNN
F 2 "" H 2750 4900 50  0001 C CNN
F 3 "" H 2750 4900 50  0001 C CNN
	1    2750 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 4800 2750 4900
Wire Wire Line
	6000 5050 6300 5050
Wire Wire Line
	6000 4950 6300 4950
Text GLabel 6300 4950 2    50   Output ~ 0
SCL
Text GLabel 6300 5050 2    50   Output ~ 0
SDA
$Comp
L Connector:Conn_01x04_Male J1
U 1 1 5FA625A6
P 2500 7000
F 0 "J1" H 2600 6600 50  0000 R CNN
F 1 "Conn_01x04_Male" H 2600 6700 50  0000 R CNN
F 2 "Connector_JST:JST_GH_SM04B-GHS-TB_1x04-1MP_P1.25mm_Horizontal" H 2500 7000 50  0001 C CNN
F 3 "~" H 2500 7000 50  0001 C CNN
	1    2500 7000
	1    0    0    1   
$EndComp
$Comp
L power:+3V3 #PWR0113
U 1 1 5FA8AA89
P 3200 6800
F 0 "#PWR0113" H 3200 6650 50  0001 C CNN
F 1 "+3V3" V 3150 7000 50  0000 C CNN
F 2 "" H 3200 6800 50  0001 C CNN
F 3 "" H 3200 6800 50  0001 C CNN
	1    3200 6800
	0    1    1    0   
$EndComp
Wire Wire Line
	3200 6800 2700 6800
Wire Wire Line
	2700 6900 3200 6900
Wire Wire Line
	2700 7100 3200 7100
$Comp
L Connector:TestPoint TP1
U 1 1 5FB06876
P 1000 4550
F 0 "TP1" V 1000 4738 50  0000 L CNN
F 1 "TestPoint" V 1045 4738 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 1200 4550 50  0001 C CNN
F 3 "~" H 1200 4550 50  0001 C CNN
	1    1000 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR060
U 1 1 5FB0687C
P 1000 4650
F 0 "#PWR060" H 1000 4400 50  0001 C CNN
F 1 "GND" H 1005 4477 50  0000 C CNN
F 2 "" H 1000 4650 50  0001 C CNN
F 3 "" H 1000 4650 50  0001 C CNN
	1    1000 4650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1000 4550 1000 4650
$Comp
L Connector:TestPoint TP2
U 1 1 5FC06BCD
P 1150 4550
F 0 "TP2" V 1150 4738 50  0000 L CNN
F 1 "TestPoint" V 1195 4738 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 1350 4550 50  0001 C CNN
F 3 "~" H 1350 4550 50  0001 C CNN
	1    1150 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR061
U 1 1 5FC06BD3
P 1150 4650
F 0 "#PWR061" H 1150 4400 50  0001 C CNN
F 1 "GND" H 1155 4477 50  0000 C CNN
F 2 "" H 1150 4650 50  0001 C CNN
F 3 "" H 1150 4650 50  0001 C CNN
	1    1150 4650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1150 4550 1150 4650
$Comp
L Device:C C33
U 1 1 5FC1D8E8
P 3150 4650
F 0 "C33" H 3035 4604 50  0000 R CNN
F 1 "0.1uF" H 3035 4695 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3188 4500 50  0001 C CNN
F 3 "~" H 3150 4650 50  0001 C CNN
	1    3150 4650
	-1   0    0    1   
$EndComp
Wire Wire Line
	2750 4400 3150 4400
Wire Wire Line
	3150 4400 3150 4500
Wire Wire Line
	3150 4800 3150 4900
$Comp
L power:GND #PWR0114
U 1 1 5FC3A23D
P 3150 4900
F 0 "#PWR0114" H 3150 4650 50  0001 C CNN
F 1 "GND" H 3155 4727 50  0000 C CNN
F 2 "" H 3150 4900 50  0001 C CNN
F 3 "" H 3150 4900 50  0001 C CNN
	1    3150 4900
	1    0    0    -1  
$EndComp
Connection ~ 3150 4400
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5FC5AEA5
P 3250 4400
F 0 "#FLG0102" H 3250 4475 50  0001 C CNN
F 1 "PWR_FLAG" H 3250 4573 50  0000 C CNN
F 2 "" H 3250 4400 50  0001 C CNN
F 3 "~" H 3250 4400 50  0001 C CNN
	1    3250 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	3250 4400 3150 4400
$Comp
L power:GND #PWR0115
U 1 1 5FCBD429
P 4750 7050
F 0 "#PWR0115" H 4750 6800 50  0001 C CNN
F 1 "GND" V 4755 6922 50  0000 R CNN
F 2 "" H 4750 7050 50  0001 C CNN
F 3 "" H 4750 7050 50  0001 C CNN
	1    4750 7050
	0    1    1    0   
$EndComp
Wire Bus Line
	7700 1500 7700 2300
Wire Bus Line
	3900 4150 3900 5550
Wire Bus Line
	6800 3750 6800 5350
Wire Bus Line
	9450 3350 9450 5850
$EndSCHEMATC
